---
title: "Location, Location, Location"
author: "Adnan Fiaz"
date: "25 January 2018"
ratio: 16x10
output:
  rmdshower::shower_presentation:
    self_contained: false
    katex: true
    theme: mango
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, cache=TRUE, message=FALSE, warning=FALSE)
google_api_key <- readLines("google_api_key.txt")

parse_result <- function(result){
  name <- result$results$name
  location <- result$results$geometry$location
  return(cbind("name"=name, location))
}
```


##  

<img src="front.JPG" class="cover">


## Introduction

* Who am I
    + Senior Data Scientist
    + Projects in Government and Manufacturing
    + Experience with R and Python
* What is this about
    + A short exploration of spatial viz
    + Finding BirminghamR meetup venue as usecase
* What is this not about
    + Complex spatial analysis
    + The *sf* or *raster* packages

## Step 1: Where is everyone?

Question: Are there any useRs in Birmingham? <br>
Data souce: Twitter (#rstats)


```{r, eval=FALSE, echo=TRUE}
library(rtweet) # http://rtweet.info for more information
library(tidyverse)

# get all tweets that mention #rstats
usrs <- search_tweets("#rstats", n = 18000) %>% 
  # extract user data from the tweets
  users_data() %>% 
  select(user_id, location) %>% 
  # drop users with no location
  filter(location != "") %>%
  # join together with the following (not everyone tweets)
  union(
    # get user data from users that mention #rstats in their profile
    search_users("#rstats", n=1000) %>% 
    select(user_id, location) %>% 
    filter(location != "")
  ) %>% 
  # drop duplicates
  distinct()
```

```{r}
# saving results to prevent re-querying twitter API, knitr caching is unpredictable
# saveRDS(usrs, "rstats_twitter_users_24012018.Rds")
usrs <- readRDS("rstats_twitter_users_24012018.Rds")
```

## Step 1: Where is everyone?
```{r}
kable(head(usrs))
```

## Step 1: Where is everyone?

To enable visualisation we need more than the location, we need coordinates.

```{r, eval=FALSE, echo=TRUE}
library(ggmap) # https://github.com/dkahle/ggmap for more info
# register a google api key to overcome quota limit
register_google(key=google_api_key)
# get the longitude/latitude 
usrs <- usrs %>%
  # limiting to the first 1500 users to not use up the quota
  slice(1:1500) %>%  
  mutate_geocode(location) %>% 
  # drop failed requests
  filter(!is.na(lat))
```

```{r}
# saving results to prevent re-querying google API, knitr caching is unpredictable
#saveRDS(usrs, "users_with_geocode_24012018.Rds")
usrs <- readRDS("users_with_geocode_24012018.Rds")
```

## Step 1: Where is everyone?

```{r, echo=TRUE}
qmplot(x=lon, y=lat, data=usrs, maptype="toner-lite", color = I("blue"), extent="device")
```

## Step 1: Where is everyone?

```{r}
uk <- c(left = -10, bottom = 50, right = 3, top = 58)
uk_map <- get_stamenmap(uk, zoom = 7,  maptype = "toner-lite")
ggmap(uk_map, extent="device") + 
  geom_point(aes(x=lon, y=lat), data=usrs, color=I('blue'))
```

## Step 2: What are my options?

Question: where can I hold a meetup? <br>
Data source: Google Maps / Places

```{r, echo=TRUE}
library(googleway) # https://github.com/SymbolixAU/googleway for more info
# search for places within a radius of a location
center_of_birmingham <- c(52.483056,-1.893611)
function_rooms <- google_places(location = center_of_birmingham,
                    keyword = "function room", radius = 5000,
                    key = google_api_key)
```

```{r}
function_rooms <- function_rooms %>% 
  parse_result() %>% 
  # adding type for visualisation
  mutate(type="function_room")
```

## Step 2: What are my options?

```{r}
kable(select(function_rooms, name, lat, lng))
```


## Step 2: What are my options?

```{r}
# need to reverse the coordinates for ggmap
google_center_of_birmingham <- c(-1.893611, 52.483056)
bham_map <- get_googlemap(center=google_center_of_birmingham, zoom=14, maptype = "roadmap",
              style = c(feature = "poi", element = "labels", visibility = "off")) %>% 
  ggmap(extent="device")
```

```{r}
bham_map +
  geom_point(aes(x=lng, y=lat), data=function_rooms, color=I('red'), 
             shape=I(17))
```


## Step 2: What are my options?
```{r}
pubs <- google_places(location = center_of_birmingham,
                    keyword = "pub", radius = 5000,
                    key = google_api_key) %>%
  parse_result() %>% 
  mutate(type="pub")

options <- union(function_rooms, pubs)
```

```{r}
bham_map +
  geom_point(aes(x=lng, y=lat, colour=type, shape=type), data=options) +
  scale_shape_manual(values= c(17, 16)) +
  scale_colour_manual(values= c('red', 'darkgreen')) +
  guides(scale=FALSE)
```

## Step 2: What are my options?
```{r}
uni <- google_places(location = center_of_birmingham,
                    keyword = "university", radius = 5000,
                    key = google_api_key) %>%
  parse_result() %>% 
  mutate(type="university")

options <- union(options, uni)
```

```{r}
bham_map +
  geom_point(aes(x=lng, y=lat, colour=type, shape=type), data=options) +
  scale_shape_manual(values= c(17, 16, 15)) +
  scale_colour_manual(values= c('red', 'darkgreen', 'purple')) +
  guides(scale=FALSE)
```

## Step 3: How do I get there?

Question: Are there any good transport links near the venues? <br>
Data Source: Google Maps
```{r, echo=TRUE}
train_stations <- google_places(location = center_of_birmingham,
                    place_type = "train_station", radius = 5000,
                    key = google_api_key) %>% 
  parse_result() %>%  
  mutate(type="train_station")
```

## Step 3: How do I get there?
```{r}
bham_map +
  geom_point(aes(x=lng, y=lat), data=train_stations, color=I('black'), shape=I(18))
```

## Step 3: How do I get there?
```{r}
car_parks <- google_places(location = c(52.483056,-1.893611),
                    keyword = "car park", place_type = "parking",
                    radius = 5000, key = google_api_key) %>% 
  parse_result() %>% 
  mutate(type="car_park")

transport <- union(train_stations, car_parks)

bham_map +
  geom_point(aes(x=lng, y=lat, colour=type, shape=type), data=transport) +
  scale_shape_manual(values= c(15, 18)) +
  scale_colour_manual(values= c('orange', 'black')) +
  guides(scale=FALSE)
```

## Step 4: Putting it all together

```{r}
options_transport <- union(options, transport)
options_transport$type <- factor(options_transport$type, 
                                    levels = c('function_room', 'pub', 'university', 
                                               'car_park', 'train_station'))
all_plotted <- bham_map +
  geom_point(aes(x=lng, y=lat, colour=type, shape=type), data=options_transport) +
  scale_shape_manual(values= c(17, 16, 15, 15, 18)) +
  scale_colour_manual(values= c('red', 'darkgreen', 'purple', 'orange', 'black')) +
  guides(scale=FALSE)

all_plotted
```

## Step 4: Putting it all together

```{r}
circles <- data_frame(name = rep(transport$name, each = 100),
                      angle = rep(seq(0, 2*pi, length.out = 100), nrow(transport)))

meanLatitude <- mean(transport$lat)

distance_km <- 0.3

my_circles <- transport %>% 
  # length per longitude changes with latitude, so need correction
  mutate(radiusLon = distance_km/111/cos(meanLatitude/57.3),
         radiusLat = distance_km/111) %>% 
  left_join(circles) %>% 
  mutate(longitude = lng + radiusLon * cos(angle),
         latitude = lat + radiusLat * sin(angle))

all_plotted + 
  geom_polygon(data = my_circles, aes(longitude, latitude, group = name), 
                           colour = I('black'), alpha = 0)
```

## Step 4: Putting it all together (the easy way)

### Calculate distances
```{r, echo=TRUE}
get_distances <- function(origins, destinations){
  # query the Maps Distance API
  dis <- google_distance(origins, destinations, key = google_api_key, mode="walking")
  
  # check if the query returns any results
  if("distance" %in% names(dis$rows$elements[[1]])){
    return(dis$rows$elements[[1]]$distance$value)
  } 
  return(NA)
}
```

## Step 4: Putting it all together (the easy way)

### Filter and sort results

```{r, echo=TRUE, eval=FALSE}
# for each option, calculate the distance to all transport links
result <- map2(options$lat, options$lng, 
               ~ get_distances(c(.x, .y), transport[, c("lat", "lng")])) %>% 
  # rbind all the vectors
  do.call(rbind, .) %>% 
  # convert to data.frame
  as.data.frame() %>% 
  # add the name of each option
  cbind("name"=options$name) %>%
  # transpose the data.frame to get one [option, transport, distance] combo
  gather(key="transport", value="distance", -name) %>% 
  filter(!is.na(distance), distance < 500) %>% 
  count(name) %>%
  top_n(3, wt=n)
```

```{r}
# saveRDS(result, "top_options.Rds")
result <- readRDS("top_options.Rds")
```

## Step 4: Putting it all together (the easy way)
```{r}
kable(result)
```

## Summary

- All you need is coordinates
- Working with API doesn't have to be difficult
- Don't underestimate the package ecosystem
- Don't hit your quota

Questions?


